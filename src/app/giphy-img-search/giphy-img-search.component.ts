import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

// Observable class extensions
import 'rxjs/add/observable/of';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

import { GiphyImgSearchService } from '../giphy-img-search.service';
import { GiphyImg } from '../giphy-img';

@Component({
  selector: 'app-giphy-img-search',
  templateUrl: './giphy-img-search.component.html',
  styleUrls: ['./giphy-img-search.component.scss'],
  providers: [GiphyImgSearchService]
})
export class GiphyImgSearchComponent implements OnInit {
  giphyImgs: Observable<GiphyImg[]>;
  private searchTerms = new Subject<string>();
  @Output() notify: EventEmitter<GiphyImg[]> = new EventEmitter<GiphyImg[]>();

  constructor(
    private giphyImgSearchService: GiphyImgSearchService) {}

  // Push a search term into the observable stream.
  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.giphyImgs = this.searchTerms
      .debounceTime(300)        // wait 300ms after each keystroke before considering the term
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .switchMap(term => term   // switch to new observable each time the term changes
        // return the http search observable
        ? this.giphyImgSearchService.search(term)
        // or the observable of empty heroes if there was no search term
        : Observable.of<GiphyImg[]>([]))
      .catch(error => {

        console.log(error);
        return Observable.of<GiphyImg[]>([]);
      });

    this.giphyImgs.subscribe(giphyImgs => {
      this.notify.emit(giphyImgs);
    });
  }
}
