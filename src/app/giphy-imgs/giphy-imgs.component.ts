import { Component, OnInit, Input} from '@angular/core';

import { GiphyImg } from '../giphy-img';
import { GiphyImgService } from '../giphy-img.service';

@Component({
  selector: 'app-giphy-imgs',
  templateUrl: './giphy-imgs.component.html',
  styleUrls: ['./giphy-imgs.component.scss']
})
export class GiphyImgsComponent implements OnInit {
  @Input() giphyImgs: GiphyImg[];
  @Input() searchVisible = true;
  @Input() infiniteScrollDisabled = false;
  selectedImg: GiphyImg;
  offset = 0;
  offsetChange = 25;
  currentTop = [0, 0, 0, 0];
  myCollection = JSON.parse(localStorage.getItem('myCollection')) || {};

  constructor(
    private GiphyImgService: GiphyImgService) { }

  onNotify(GiphyImg): void {
    this.giphyImgs = GiphyImg;
    if (GiphyImg.length === 0) {
      this.getImgs();
    }
  }

  styles(i) {
    this.currentTop[i % 4] += 200;

    return {
      left: (i % 4 * 25) + '%',
      top: this.currentTop[i % 4] + 'px'
    };
  }

  getImgs(): void {
    if (this.infiniteScrollDisabled) return;

    this.GiphyImgService
      .getImgs(this.offset)
      .then(imgs => {
        if (this.giphyImgs) {
          this.giphyImgs = this.giphyImgs.concat(imgs);
        } else {
          this.giphyImgs = imgs;
        }
        this.offset += this.offsetChange;
      });
  }
  ngOnInit(): void {
    this.getImgs();
  }

  onSelect(img: GiphyImg): void {
    this.selectedImg = img;
  }

  onPlus(img: GiphyImg): void {
    this.myCollection = JSON.parse(localStorage.getItem('myCollection')) || {};
    this.myCollection[img.id] = '1';
    localStorage.setItem('myCollection', JSON.stringify(this.myCollection));
  }

  onScrollDown() {
    this.getImgs();
  }

  onScrollUp() {}

}
