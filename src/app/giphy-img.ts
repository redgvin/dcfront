export class GiphyImg {
  id: number;
  import_datetime: string;
  username: string;
  tags: string;
  name: string;
  images: object;
}
