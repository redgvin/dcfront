import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { GiphyImg } from './giphy-img';

@Injectable()
export class GiphyImgSearchService {

  constructor(private http: Http) {}

  search(term: string): Observable<GiphyImg[]> {
    return this.http
      .get(`http://api.giphy.com/v1/gifs/search?q=${term}&api_key=dc6zaTOxFJmzC`)
      .map(response => response.json().data as GiphyImg[]);
  }

  byIds(ids: string): Observable<GiphyImg[]> {
    return this.http
      .get(`http://api.giphy.com/v1/gifs?ids=${ids}&api_key=dc6zaTOxFJmzC`)
      .map(response => response.json().data as GiphyImg[]);
  }

}
