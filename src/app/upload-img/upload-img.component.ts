import {Component, NgZone, OnInit} from '@angular/core';

import { UploadService } from '../upload.service';

@Component({
  selector: 'app-upload-img',
  templateUrl: './upload-img.component.html',
  styleUrls: ['./upload-img.component.scss'],
  providers: [ UploadService ]
})

export class UploadImgComponent implements OnInit {
  id: string;
  message: string;
  progress: string;

  constructor(private service: UploadService, private zone: NgZone) {
    this.service.progress$.subscribe(
      data => {
        this.zone.run(() => {
          if (data !== 100) {
            this.progress = 'progress = ' + data;
          } else {
            this.progress = '';
          }

        });
      });
  }

  dateValidate(date: Date) {
    const oneYrBefore = new Date();
    oneYrBefore.setFullYear(oneYrBefore.getFullYear() - 1);

    if (oneYrBefore > date) {
      this.message = 'Date of gif too old';

      return false
    }

    return true;
  }

  onClick(elem, username, tags) {
    const files = elem.files;

    if (!files.length) {
      this.message = 'File not selected';

      return;
    }

    if (!this.dateValidate(files[0].lastModifiedDate)) return;

    this.message = '';
    this.service.makeFileRequest('http://upload.giphy.com/v1/gifs', [username, tags], files).subscribe((response) => {
      const myCollection = JSON.parse(localStorage.getItem('myCollection')) || {};
      myCollection[response.data.id] = '1';
      localStorage.setItem('myCollection', JSON.stringify(myCollection));
      this.id = response.data.id;

      this.message =  `File with id ${this.id} upload successfully`;
    });
  }

  ngOnInit() {
  }
}
