import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.scss']
})
export class ImageComponent implements OnInit {
  @Input() imagePath: string;
  @Input() addClass: string;
  isLoaded: boolean;
  el: ElementRef;
  image: HTMLElement;

  constructor(el: ElementRef) {
    this.el = el;
  }

  ngOnInit() {
    this.image = this.el.nativeElement.querySelector('img');
    this.loadImage(this.imagePath)
      .do(() => this.setImage(this.image, this.imagePath))
      .subscribe();
  }

  loadImage(imagePath: string): Observable<HTMLImageElement> {
    return Observable
      .create(observer => {

        const img = new Image();
        img.src = imagePath;
        img.onload = () => {
          observer.next(imagePath);
          observer.complete();
        };
        img.onerror = err => {
          observer.error(null);
        };
      });
  }

  setImage(element: HTMLElement, imagePath: string) {
    const isImgNode = element.nodeName.toLowerCase() === 'img';
    if (isImgNode) {
      (<HTMLImageElement>element).src = imagePath;
      this.isLoaded = true
    }

    return element;
  }
}
