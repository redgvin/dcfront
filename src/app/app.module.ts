import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { AppRoutingModule } from './app-routing/app-routing.module';

import { AppComponent } from './app.component';
import { GiphyImgSearchComponent } from './giphy-img-search/giphy-img-search.component';
import { GiphyImgsComponent } from './giphy-imgs/giphy-imgs.component';

import { GiphyImgService } from './giphy-img.service';
import { DialogComponent } from './dialog/dialog.component';
import { UploadImgComponent } from './upload-img/upload-img.component';
import { CollectionComponent } from './collection/collection.component';
import { ImageComponent } from './image/image.component';

@NgModule({
  declarations: [
    AppComponent,
    GiphyImgSearchComponent,
    GiphyImgsComponent,
    DialogComponent,
    UploadImgComponent,
    CollectionComponent,
    ImageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    InfiniteScrollModule,
    AppRoutingModule
  ],
  providers: [GiphyImgService],
  bootstrap: [AppComponent]
})
export class AppModule { }
