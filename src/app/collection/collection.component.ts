import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { GiphyImgSearchService } from '../giphy-img-search.service';
import { GiphyImg } from '../giphy-img';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss'],
  providers: [GiphyImgSearchService]
})
export class CollectionComponent implements OnInit {
  giphyImgsObservable: Observable<GiphyImg[]>;
  giphyImgs: GiphyImg[];
  myCollection = JSON.parse(localStorage.getItem('myCollection')) || {};
  @Output() notify: EventEmitter<GiphyImg[]> = new EventEmitter<GiphyImg[]>();

  constructor(
    private giphyImgSearchService: GiphyImgSearchService) {}

  ngOnInit(): void {
    this.giphyImgsObservable = this.giphyImgSearchService.byIds(Object.keys(this.myCollection).join(','));

    this.giphyImgsObservable.subscribe(giphyImgs => {
      this.giphyImgs = giphyImgs;
    });
  }

}
