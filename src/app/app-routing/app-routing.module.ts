import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GiphyImgsComponent } from '../giphy-imgs/giphy-imgs.component';
import { UploadImgComponent } from '../upload-img/upload-img.component';
import { CollectionComponent } from '../collection/collection.component';

const routes: Routes = [
  { path: '', redirectTo: '/list', pathMatch: 'full' },
  { path: 'list',  component: GiphyImgsComponent },
  { path: 'upload',  component: UploadImgComponent },
  { path: 'collection',  component: CollectionComponent }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
