import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { GiphyImg } from './giphy-img';

@Injectable()
export class GiphyImgService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private url = `http://api.giphy.com/v1/gifs/`;
  private apiKey = 'api_key=dc6zaTOxFJmzC';
  private trandingUrl = `${this.url}trending?${this.apiKey}`;

  constructor(private http: Http) { }

  getImgs(offset): Promise<GiphyImg[]> {
    return this.http.get(`${this.trandingUrl}&offset=${offset}`)
      .toPromise()
      .then(response => response.json().data as GiphyImg[])
      .catch(this.handleError);
  }

  getImg(id: number): Promise<GiphyImg> {
    const url = `${this.url}/${id}?${this.apiKey}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as GiphyImg)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
