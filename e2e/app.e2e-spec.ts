import { DcfrontPage } from './app.po';

describe('dcfront App', () => {
  let page: DcfrontPage;

  beforeEach(() => {
    page = new DcfrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
